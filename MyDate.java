public class MyDate {
    int day;
    int mounth;
    int year;

    public MyDate(int d,int m,int y) {
        day = d;
        mounth = m;
        year = y;
        if (this.day==32){
            this.mounth+=1;
            this.day=1;
        }
        if ((this.mounth == 2) && (this.day == 29)) {
            this.mounth += 1;
            this.day = 1;
        }
        if ((this.mounth == 12) && (this.day == 32)) {
            this.year+=1;
            this.day=1;
            this.mounth=1;
        }
        if ((this.mounth == 3) && (this.day == 0)) {
            this.mounth -= 1;
            this.day = 28;
        }
        if (this.mounth == 13) {
            this.year+=1;
            this.mounth=1;
        }

    }


    public String toString(){
        return this.day + "-" + this.mounth + "-" + this.year;
    }

    public void incrementDay(){
        this.day=this.day+1;
        if ((this.mounth == 12) && (this.day == 32)) {
            this.year+=1;
            this.day=1;
            this.mounth=1;
        }
        if ((this.mounth == 2) && (this.day == 29)) {
            this.mounth += 1;
            this.day = 1;
        }
        else if (this.day==32){
            this.mounth+=1;
            this.day=1;
        }
    }
    public  void incrementDay(int i){
        this.day=this.day+i;
        if ((this.mounth == 12) && (this.day == 32)) {
            this.year+=1;
            this.day=1;
            this.mounth=1;
        }
        if ((this.mounth == 2) && (this.day == 29)) {
            this.mounth += 1;
            this.day = 1;
        }
        else if (this.day==32){
            this.mounth+=1;
            this.day=1;
        }
    }
    public void incrementYear(){
        this.year=this.year+1;
    }
    public void incrementYear(int i){
        this.year=this.year+i;
    }
    public void decrementYear(){
        this.year=this.year-1;
    }
    public void decrementYear(int i){
        this.year=this.year-i;
    }
    public void decrementDay(){
        this.day=this.day-1;
        if ((this.mounth == 3) && (this.day == 0)) {
            this.mounth -= 1;
            this.day = 28;
        }
        if ((this.mounth == 1) && (this.day == 0)) {
            this.year-=1;
            this.day=31;
            this.mounth=12;
        }
    }
    public void decrementDay(int i){
        this.day=this.day-i;
        if ((this.mounth == 3) && (this.day == 0)) {
            this.mounth -= 1;
            this.day = 28;
        }
        if ((this.mounth == 1) && (this.day == 0)) {
            this.year-=1;
            this.day=31;
            this.mounth=12;
        }
    }
    public void incrementMonth(){
        this.mounth=this.mounth+1;
        if (this.mounth == 13) {
            this.year+=1;
            this.mounth=1;
        }
    }
    public void incrementMonth(int i){
        this.mounth=this.mounth+i;
        if (this.mounth == 13) {
            this.year+=1;
            this.mounth=1;
        }
    }
    public void decrementMonth(){
        this.mounth=this.mounth-1;
        if (this.mounth == 0) {
            this.year-=1;
            this.mounth=12;
        }
    }
    public void decrementMonth(int i){
        this.mounth=this.mounth-i;
        if (this.mounth == 0) {
            this.year-=1;
            this.mounth=12-i;
        }
    }
    public boolean isBefore(MyDate d){
        int a=0;
        if (a==0){
            if (this.year<d.year){
                a=1;
            }
            else if (this.year>d.year){
                a=0;
            }
            else if (this.year==d.year){
                if (this.mounth<d.mounth){
                    a=1;
                }
                else if (this.mounth>d.mounth){
                    a=0;
                }
                else if (this.mounth==d.mounth){
                    if (this.day<d.day){
                        a=1;
                    }
                    else {
                        a=0;
                    }
                }
            }
        }
        if (a==1){
            return true;
        }
        else {
            return false;
        }


    }
    public boolean isAfter(MyDate d){
        int a=0;
        if (a==0){
            if (this.year>d.year){
                a=1;
            }
            else if (this.year<d.year){
                a=0;
            }
            else if (this.year==d.year){
                if (this.mounth>d.mounth){
                    a=1;
                }
                else if (this.mounth<d.mounth){
                    a=0;
                }
                else if (this.mounth==d.mounth){
                    if (this.day>d.day){
                        a=1;
                    }
                    else {
                        a=0;
                    }
                }
            }
        }
        if (a==1){
            return true;
        }
        else {
            return false;
        }
    }
    public Integer dayDifference(MyDate d){
        int sonuc=0;
        int yearDiff=(this.year-d.year)*365;
        int mounthDiff=(this.mounth-d.mounth)*30;
        int dayDiff=(this.day-d.day);
        sonuc=yearDiff+mounthDiff+dayDiff;
        return sonuc;
    }

}
